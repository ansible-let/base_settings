# Ansible Collection - let.base_settings

Server settings for LET Servers.

The role base-settings contains:
- authorization, SSH settings and public keys
- environment variables
- ETH NTP timeserver and timezone
- login message based on SnipeIT information
- install fail2ban rules (not yet...)
- disable SELinux
- install standard tools (htop, vim, etc.)
- disable IPv6
- configure firewalld (`roles/firewall`)
- obtain Let's Encrypt certificates
