Authorization
=========

Configures the SSH daemon to allow key based logins only, and manages the public keys for *root* access.

Requirements
------------

None.

SSH Public Key Files
--------------

Keys provided have to be stored in ```files/ssh_keys```, and added to the corresponding task.

A task for removal of obsolete keys is also available. Just move a key from the deployment task
to the purge task.

