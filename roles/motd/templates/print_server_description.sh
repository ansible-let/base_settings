#!/usr/bin/env bash

###
#
# Get server description items from SnipeIT assets and print
# server relevant informations at login
# 
# Requirements: - lsb_release of pkg redhat-lsb-core
#
# Vers. 0.9  : bm-20210810 - public beta
#       0.91 : bm-20210906 - fixed: nagios user fails to execute checks by ssh
#       0.92 : bm-20210907 - fixed in issue which prevented from composing the
#                            last login message correctly in RHEL 8
#       0.93 : bm-20210914 - it's now possible the prevent from displaying the
#                            server description not only for user 'nagios'
#       0.94 : bm-20210930 - check whether 'jq' and 'lsb_release' are installed,
#                            print an installation hint otherwise
#       1.0  : bg-20211223 - adapted for Ansible
#
# Todo: - display better information when server has no SnipeIT record --> done
#       - either show logged in users or last login
#       - consider unmanaged servers --> done
#       - print additionalAttributes?
#       - handle special characters like apostrophes (') in SnipeIT notes
#
###

#set -x

### Configuration: add 'su users' to array below
#
## Define a list of users as an array. For these users displaying server informations at login is disabled.
## DON'T delete user 'nagios' in the array, as nagios checks by ssh will fail otherwise!
#
exceptUsers=(nagios postgres gwos)

### Functions
#
printwelcomeheader () {
    kernelVersion=$( uname -r )
    osDistribution=$( lsb_release -i /dev/null 2>&1 | awk 'NR==1{print $3}' )
    if [[ "$osDistribution" == *"RedHat"* ]]; then
       osDistribution="RHEL"
    elif [[ "$osDistribution" == *"Debian"* ]]; then
       osDistribution="Debian"
    fi
    osVersion=$( lsb_release -r | awk 'NR==1{print $2}' )
    #
    welcomeString="Welcome to $HOSTNAME --- $osDistribution $osVersion --- Kernel $kernelVersion"
    #
    echo $welcomeString
    {% raw %}
    printf '%*s\n' ${#welcomeString} '' | tr ' ' '='
    {% endraw %}
    echo
}
#
printheader () {
    if [ $( pgrep -f -c salt-minion ) -gt 0 ]; then # server is managed by saltstack
        echo -e "This Server is managed by \e[00;31msaltstack\e[00m and has the following roles:"
        echo
        echo "$( egrep "^\s+-{1}\s\w" "/etc/salt/grains" | tr -s "\n" ";" | column -t -s ";" )"
    elif [ -d $cacheFileDir ] && [ -f $cacheFileDir/$cacheFile ] && [[ $( sed -n 10p $cacheFileDir/$cacheFile ) = "null" ]]; then # server is unmanaged
        echo -e "This Server is \e[00;31munmanaged\e[00m."
    else # server is managed by Ansible
        echo -e "This Server is managed by \e[00;31mAnsible\e[00m and is member of the following groups:"
    fi
    echo
}
#
printitems () {
    cat <<EOF
{% for group in group_names %}
 - {{ group }}
{% endfor %}

Environment: {{ Umgebung }}             Boss: {{ Hauptverantwortlicher }} ({{ Nebenverantwortlicher }})

Service: {{ Service }}

Notes:
{{ notes }}

EOF
}

### Main
#
loginUser=$( whoami )
#
if [[ ! " ${exceptUsers[*]} " =~ " ${loginUser} " ]]; then # prevent script from beeing executed for users in $exceptUsers
    clear
    printwelcomeheader
    #
    if ! [ -x "$( command -v lsb_release )" ]; then # test whether mandatory commands are available
        echo "To display server informations 'lsb_release' needs to be installed on this server!"
    else
        ## Print server informations
        printheader
        printitems
    fi
    #
    ## Recompose last login message from 'last' command
    SAVEIFS=$IFS
    IFS=' '
    lastLogin=($( last -i | head -n2 | tail -n1  )) # last login before this login is the second line in the  output of 'last' command!
    IFS=$SAVEIFS
    lLMessage="Last Login: ${lastLogin[0]} ${lastLogin[3]} ${lastLogin[5]} ${lastLogin[4]} ${lastLogin[6]} from ${lastLogin[2]}"
    #
    ## Print last login message
    echo "$lLMessage"
    echo
fi
