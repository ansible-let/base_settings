#!/usr/bin/bash
subscription-manager unregister
curl -k https://id-sat-prd-02.ethz.ch/pub/katello-rhsm-consumer | /bin/bash
MAJOR=$(grep 'REDHAT_BUGZILLA_PRODUCT_VERSION' /etc/os-release | cut -f2 -d= | cut -f1 -d.)
subscription-manager register --org="COMMON" --activationkey="let-isg-${MAJOR}-server" --force
yum install -y katello-host-tools

