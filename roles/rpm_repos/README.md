# Install/Activate RPM repos

## Purpose
Manage RPM repos after Satellite migration, to replace custom repos no more available on Satellite.

Also installs the Satellite migration script, which can be used to register a system. See also *Tags* below.

## Usage
Include role like

```
- name: Deploy RPM repos
  hosts: LET_Server
  user: root
  roles:
    - let.base_settings.rpm_repos
```

## Compatibility
- Red Hat
- AlmaLinux
- Rocky Linux

## Configuration
### Globally Enabled Repos
This list of repos is enabled on all systems:

- EPEL
- PowerTools (aka. CodeReady Builder)
- ETHZ root certificates
- TSM Backup

For Red Hat, the corresponding repo from Satellite is used. Compatible OS will use
the upstream repositories directly (use a HTTP proxy for private subnets).

### ElasticStack
Enabled on systems in Ansible groups

- ElasticCloudEnterprise
- Kibana
- CodeExpert

### Shibboleth
Enabled on all SnipeIt "Moodle_*" groups, and on group "Moodle" (for convenience,
not present in SnipeIt).

### Zammad
Enabled on systems in Ansible group "Zammad".

### PostgreSQL
Enabled if a variable `postgres_version` is defined. The variable describes the
version to be installed, i.e.

```
[postgres]
grumpy-cat

[postgres:vars]
postgres_version=14
```

### Remi
Enabled if a variable `remi_php_version` is defined. The variable describes the
version to be installed, i.e.

```
[php]
grumpy-cat

[php:vars]
remi_php_version=php80
```

Additionally, the repo "remi_safe" will be enabled. It contains depencies required
for the PHP repos, and replaces the "remi" repository. "remi_safe" is guaranteed to
not collide with system repositories.

### HP Firmware
If a baremetal HP system is detected, the firmware repository is enabled
automatically, also the repo for HP Smart Update Manager.

Also *Docker CE* has to be enabled on Groundwork servers:

### Docker CE
Enabled on systems in Ansible group "Groundwork", if on a RHEL OS major release > 7.

## Tags
Tags can be used to control some of the tasks. Tags can be set on the commandline:

```bash
ansible-playbook -l myserver -t my_tag my_playbook.yml
```

### Suppress Deployment of Migration Script
```bash
ansible-playbook -l myserver -t untagged my_playbook.yml
```

### Deploy Migration Script Only
```bash
ansible-playbook -l myserver -t deploy_migration my_playbook.yml
```

### Deploy Migration Script and Migrate
```bash
ansible-playbook -l myserver -t do_migration my_playbook.yml
```
