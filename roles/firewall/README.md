Role Name
=========

Firewalld configuration for LET environment.

Role Variables
--------------

- `firewalld_zone`: zone used as default, set to `drop`

Firewall Sets
-------------

Currently (17. Feb. 2022), the following set are provided for:

- Moodle
- Ilias-LDA (Testing, Production, DB)
- Elasticsearch
- Kibana
- Groundwork
- PolyBook
- OP-Portal

Each set is configured in a separate file in `vars/main/`.

Dependencies
------------

Because the Ansible builtin role cannot manage custom services, the role [ryandaniels/firewalld](https://galaxy.ansible.com/ryandaniels/firewalld) has been copied into this collection. Connection to the upstream repository has been removed. The role requires `firewalld_managed` to be set to `true` in a `vars` file; settings in `defaults` are **not** recognized.

License
-------

MIT (for `ryandaniels/firewalld`)
