---
- name: Get default zone
  ansible.builtin.command: firewall-cmd --get-default-zone
  register: default_zone
  changed_when: false

- name: Set default zone
  ansible.builtin.command: "firewall-cmd --set-default-zone {{ firewalld_zone }}"
  when: firewalld_zone not in default_zone.stdout

- name: Define custom services
  # see vars/main/services.yml
  ansible.builtin.include_role:
    name: let.base_settings.firewalld

- name: Allow ping from everywhere (wherefrom is handled by VPZ firewall)
  ansible.posix.firewalld:
    rich_rule: rule family=ipv4 icmp-type name=echo-request accept
    permanent: true
    immediate: true
    zone: "{{ firewalld_zone }}"
    state: enabled

- name: Allow SSH admin access from internal networks
  ansible.posix.firewalld:
    rich_rule: 'rule family=ipv4 source address={{ item }} service name=ssh accept'
    permanent: true
    immediate: true
    zone: "{{ firewalld_zone }}"
    state: enabled
  loop: "{{ firewalld_ssh_sources_admin }}"

- name: Skip cleanup on exit
  ansible.builtin.lineinfile:
    path: /etc/firewalld/firewalld.conf
    regexp: "^CleanupOnExit="
    line: "CleanupOnExit=no"
    owner: root
    group: root
    mode: "0600"
  notify: Restart firewalld

- name: Prohibit zone drifting
  ansible.builtin.lineinfile:
    path: /etc/firewalld/firewalld.conf
    regexp: "^AllowZoneDrifting="
    line: "AllowZoneDrifting=no"
    owner: root
    group: root
    mode: "0600"
  notify: Restart firewalld
