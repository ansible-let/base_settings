System Tools
=========

Installs basic system tools.

Requirements
------------

None.

Role Variables
--------------

Package lists are defined in `defaults`:

- `let_base_packages`: packages to be installed
- `let_obsolete_packages`: packages to be removed
- `let_base_rpm_packages`: packages with names specific to RPM
- `let_base_deb_packages`: packages with names spefific to DEB

Dependencies
------------

None.
